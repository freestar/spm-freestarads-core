// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 5.31.0
let package = Package(
    name: "FreestarAds",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds",
            targets: [
            "FreestarAds",
            "FSTRAdsIdentity",
            "LRAtsSDK",
            "ConfiantSDK",
            "FreestarAds-Core",
            ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
      url: "https://github.com/maddapper/spm-promises.git",
      "2.1.0" ..< "3.0.0"
      ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
         .binaryTarget(
            name: "FreestarAds",
            url: "https://gitlab.com/freestar/spm-freestarads-core/-/raw/5.31.0/FreestarAds.xcframework.zip",
            checksum: "754c8ef1f7797efa2f82b6134e700a640658c8973f034b0d721312dbac11f8f4"
            ),
         .binaryTarget(
            name: "FSTRAdsIdentity",
            url: "https://gitlab.com/freestar/spm-freestarads-core/-/raw/5.31.0/FSTRAdsIdentity.xcframework.zip",
            checksum: "954e09de9afee115c42e4e4556e578a87fb47bc00c03b47b570e0bd68fc3f655"
            ),
         .binaryTarget(
            name: "LRAtsSDK",
            url: "https://gitlab.com/freestar/spm-freestarads-core/-/raw/5.31.0/LRAtsSDK.xcframework.zip",
            checksum: "5f004b8ae79df6d69ec2aa4566fa89bfde1bb5bf667522a7740efc6b921c0da6"
            ),
         .binaryTarget(
            name: "ConfiantSDK",
            url: "https://gitlab.com/freestar/spm-freestarads-core/-/raw/5.31.0/ConfiantSDK.xcframework.zip",
            checksum: "94ff12a0eb3fcf98bff483a0b70053d4e07a4a0fdfd4c3665d73ea65051ed571"
            ),
         .target(
            name: "FreestarAds-Core",
                dependencies: [
                    .product(name: "FSLPromises", package: "spm-promises")
                ]
            ),
    ]
)
